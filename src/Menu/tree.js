export  const navigationTree =
    [
        {
            title: 'Shop',
            display: true,
            subjects: [
                {
                    title: 'Mobility',
                    display: true,
                    subjects: [
                        {
                            title: 'Why Bell',
                            route: 'why',
                            display: true,
                        },
                        {
                            title: 'Our network',
                            route: 'network',
                            display: true,
                        },
                        {
                            title: 'Devices',
                            route: 'Devices',
                            display: true,
                        },
                        {
                            title: 'Bring your own device',
                            route: 'BringDevice',
                            display: true,
                        },
                        {
                            title: 'Plans',
                            route: 'Plans',
                            display: true,
                        },
                        {
                            title: 'Prepaid',
                            route: 'Prepaid',
                            display: true,
                        },
                        {
                            title: 'Accessories',
                            display: true,
                        },
                        {
                            title: 'Connected things',
                            display: true,
                        }
                    ]
                },
                {
                    title: 'Bundles',
                    display: true,
                    subjects: [
                        {
                            title: 'Why Bundles',
                            display: true,
                        },
                        {
                            title: 'Our Bundles',
                            display: true,
                        }
                    ]
                },
                {
                    title: 'TV',
                    display: true
                },
                {
                    title: 'Internet',
                    display: true
                },
                {
                    title: 'Home Phone',
                    display: true
                },
                {
                    title: 'Smart Phone',
                    display: true
                },
                {
                    title: 'Promotions',
                    display: true
                }



            ]
        },
        {
            title: 'Support',
            display: true,
            subjects: [
                {
                    title: 'SUP',
                    display: true,
                    subjects: [
                        {
                            title: 'Why SUP',
                            display: true,
                        },
                        {
                            title: 'Our SUP',
                            display: true,
                        }
                    ]
                },
                {
                    title: 'Bundles',
                    display: true,
                    subjects: [
                        {
                            title: 'Why Bundles',
                            display: true,
                        },
                        {
                            title: 'Our network',
                            display: true,
                        }
                    ]
                },


            ]
        },
        {
            title: 'My Bell',
            display: true
        }
    ];
