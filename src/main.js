import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueMq from 'vue-mq'

import { routes } from './routing/routes'
Vue.config.productionTip = false;


const router = new VueRouter({
  routes // short for `routes: routes`
});
Vue.use(VueRouter);
Vue.use(VueMq, {
  breakpoints: {
    mobile: 450,
    tablet: 900,
    laptop: 1250,
    desktop: Infinity,
  }
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app');
