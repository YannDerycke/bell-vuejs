import Home from '../components/Home.vue'
import Why from '../components/Shop/Why.vue'
import Network from '../components/Shop/Network'

export const routes = [
    {
        path: '/',
        name: 'Homed',
        component: Home
    },
    {
        path: '/why',
        name: 'why',
        component: Why,
    },
    {
        path: '/network',
        name: 'network',
        component: Network,
    }
];
